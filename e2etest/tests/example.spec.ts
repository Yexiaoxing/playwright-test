import { test, expect, Page } from '@playwright/test';

test.beforeEach(async ({ page }) => {
  await page.goto('https://www.google.com');
});

test.describe('New Todo', () => {
  test('should allow me to add todo items', async ({ page }, testInfo) => {
    const output = testInfo.outputPath(
      "screenshot",
      testInfo.title + ".png"
    );

    await page.screenshot({ path: output, fullPage: true });
    await testInfo.attach(`${testInfo.title}-screenshot`, {
      path: output,
      contentType: "image/png",
    });

    throw new Error("This should error")
  });

});
